package racedump

import (
    "bufio"
    "encoding/json"
    "os"
)

var dumperActive bool

func init(){
    dumperActive = true
}

func SetDumperActive(pdumperActive bool){
    dumperActive = pdumperActive
}

func DumpMessage(f *os.File, message map[string]interface{}, messageDirection string) (err error) {
    
    if (dumperActive){
        // `bufio` provides buffered writers in addition
        // to the buffered readers we saw earlier.
        writer := bufio.NewWriter(f)

        var payload []byte

        payload, err = json.Marshal(message)
        writer.WriteString(messageDirection + ":");
        _, err = writer.Write([]byte(payload))
        writer.WriteString("\n")
        writer.Flush()
    }

    return err
}