package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"noobbot/racedomain"
	"noobbot/racedump"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

var dumpFile *os.File
var turboAvailable bool

func connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

func read_msg(reader *bufio.Reader) (msg string, err error) {
	var line string
	line, err = reader.ReadString('\n')

	if err != nil {
		return
	}

	msg = line

	return
}

func write_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)
	_, err = writer.Write([]byte(payload))
	if err != nil {
		return
	}
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()

	racedump.DumpMessage(dumpFile, m, "S")

	return
}

func create_join_data(name string, key string, trackName string, password string, carCount uint8) (data map[string]interface{}) {

	data = make(map[string]interface{})

	if trackName == "" && password == "" && carCount == 0 {

		data["name"] = name
		data["key"] = key
	} else {

		bootId := make(map[string]string)
		bootId["name"] = name
		bootId["key"] = key

		data["botId"] = bootId
		data["trackName"] = trackName
		data["password"] = password
		data["carCount"] = carCount

	}

	return data
}

func send_join(writer *bufio.Writer, name string, key string) (err error) {

	data := create_join_data(name, key, "", "", 0)
	err = write_msg(writer, "join", data)

	return
}

func send_join_race(writer *bufio.Writer, name string, key string, trackName string, password string, carCount uint8) (err error) {

	if trackName == "" && password == "" && carCount == 0 {
		send_join(writer, name, key)
	} else {
		data := create_join_data(name, key, trackName, password, carCount)
		err = write_msg(writer, "joinRace", data)
	}

	return
}

func send_ping(writer *bufio.Writer) (err error) {
	err = write_msg(writer, "ping", make(map[string]string))
	return
}

func send_throttle(writer *bufio.Writer, throttle float32) (err error) {
	err = write_msg(writer, "throttle", throttle)
	return
}

func send_switch_lane(writer *bufio.Writer, direction string) (err error) {
	err = write_msg(writer, "switchLane", direction)
	return
}

func send_turbo(writer *bufio.Writer, message string) (err error) {
	err = write_msg(writer, "turbo", message)

	return
}

func compute_throttle(throttle float32, angle float64) float32 {
	var computed_throttle float32

	computed_throttle = throttle

	if angle > 0.40 || angle < -40 {
		computed_throttle = 0.01
	}

	if angle > 0.53 || angle < -53 {
		computed_throttle = 0.2
	}

	return computed_throttle
}

func parse_json_to_struct(input string, json_struct interface{}) (err error) {

	err = json.Unmarshal([]byte(input), &json_struct)
	if err != nil {
		return
	}
	return
}

func dispatch_msg(writer *bufio.Writer, msgtype string, msg interface{}) (err error) {

	switch msgtype {
	case "join":
		log.Printf("Joined")
		send_ping(writer)
	case "joinRace":
		log.Println("Race Joined")
		send_ping(writer)
	case "gameInit":
		gameInitMsg := &racedomain.GameInitMsg{}
		parse_json_to_struct(msg.(string), gameInitMsg)
		log.Println("Game Init", gameInitMsg)
		send_ping(writer)
	case "gameStart":
		log.Println("Game started")
		send_ping(writer)
	case "crash":
		log.Printf("Someone crashed")
		send_ping(writer)
	case "gameEnd":
		log.Printf("Game ended")
		send_ping(writer)
	case "turboAvailable":
		turboAvailableMsg := &racedomain.TurboAvailableMsg{}
		parse_json_to_struct(msg.(string), turboAvailableMsg)

		turboAvailable = true
		send_ping(writer)
	case "carPositions":
		send_throttle(writer, 0.651)
	case "error":
		log.Printf(fmt.Sprintf("Got error: %v", msg))
		send_ping(writer)
	default:
		log.Printf("Got msg type: %s", msgtype)
		send_ping(writer)
	}
	return
}

func parse_gameTick(msg map[string]interface{}) float64 {
	var gameTick float64

	val, ok := msg["gameTick"]

	if !ok {
		return -1
	}

	gameTick, ok = val.(float64)

	return gameTick
}

func parse_and_dispatch_input(writer *bufio.Writer, input string) (err error) {

	var msgMarshalled interface{}

	err = json.Unmarshal([]byte(input), &msgMarshalled)
	if err != nil {
		err = errors.New(fmt.Sprintf("Invalid message : %T", input))
		return
	}

	var msg map[string]interface{}
	var ok bool
	msg, ok = msgMarshalled.(map[string]interface{})

	if !ok {
		err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
		return
	}

	err = racedump.DumpMessage(dumpFile, msg, "R")
	if err != nil {
		log.Printf("Error dumping file", err)
		return
	}

	err = dispatch_msg(writer, msg["msgType"].(string), input)
	if err != nil {
		return
	}

	return
}

func bot_loop(conn net.Conn, name string, key string, trackName string, password string, carCount uint8) (err error) {
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)

	send_join_race(writer, name, key, trackName, password, carCount)

	for {
		input, err := read_msg(reader)
		if err != nil {
			log_and_exit(err)
		}
		err = parse_and_dispatch_input(writer, input)
		if err != nil {
			log_and_exit(err)
		}
	}
}

func parse_args() (host string, port int, name string, key string, trackName string, password string, carCount uint8, dumperActive bool, err error) {
	args := os.Args[1:]
	if len(args) < 4 {
		return "", 0, "", "", "", "", 0, false, errors.New("Usage: ./run host port botname botkey [trackName password carCount dumperActive]")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", "", "", 0, false, errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
	}
	name = args[2]
	key = args[3]
	dumperActive = false

	if len(args) >= 4 {
		trackName = args[4]
		password = args[5]
		var carCountint int
		carCountint, err = strconv.Atoi(args[6])
		carCount = uint8(carCountint)

		if err != nil {
			return "", 0, "", "", "", "", 0, false, errors.New(fmt.Sprintf("Could not parse car count value to integer: %v\n", args[6]))
		}
		if len(args) >= 7 {
			dumperActive, err = strconv.ParseBool(args[7])
			if err != nil {
				return "", 0, "", "", "", "", 0, false, errors.New(fmt.Sprintf("Could not parse dumper Active value to bool: %v\n", args[7]))
			}
		}

	}

	return
}

func log_and_exit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

func main() {
	turboAvailable = false
	host, port, name, key, trackName, password, carCount, dumperActive, err := parse_args()

	if err != nil {
		log_and_exit(err)
	}

	// Get the path to the executable. There we will create a dumpFile if dumping is activated
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v, track name=%v, password=%v, carCount=%v\n", host, port, name, key, trackName, password, carCount)

	conn, err := connect(host, port)

	if err != nil {
		log_and_exit(err)
	}
	defer conn.Close()

	t := time.Now()
	tStr := t.Format("2006-01-02")

	racedump.SetDumperActive(dumperActive)

	if dumperActive {
		dumpFilePath := dir + "/racedumps/racedump_" + name + "_" + tStr + ".txt"
		log.Printf("Creating dump race file: %v", dumpFilePath)
		// For more granular writes, open a file for writing.
		dumpFile, err = os.Create(dumpFilePath)
		if err != nil {
			log_and_exit(err)
		}
		// It's idiomatic to defer a `Close` immediately
		// after opening a file.
		defer dumpFile.Close()
	}

	err = bot_loop(conn, name, key, trackName, password, carCount)
}
