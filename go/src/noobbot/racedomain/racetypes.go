package racedomain

type msgType string

type gameId string

type BotId struct {
	Key  string
	Name string
}

type JoinRace struct {
	BotId     BotId
	CarCount  uint8
	Password  string
	TrackName string
}

type JoinRaceMsg struct {
	Data    JoinRace
	MsgType msgType
}

type PingMsg struct {
	Data    interface{}
	MsgType msgType
}

type YourCar struct {
	Color string
	Name  string
}

type YourCarMsg struct {
	YourCar YourCar
	GameId  gameId
	MsgType msgType
}

type Dimensions struct {
	GuideFlagPosition int32
	Length            int32
	Width             int32
}

type CarId struct {
	Color string
	Name  string
}

type Car struct {
	Dimensions Dimensions
	Id         CarId
}

type RaceSession struct {
	Laps         int32
	MaxLapTimeMs int32
	QuickRace    bool
}

type Lane struct {
	DistanceFromCenter int32
	index              int32
}

type Piece struct {
	Length float32
	Switch bool
	angle  float32
	radius float32
}
type Position struct {
	X int32
	Y int32
}
type StartingPoint struct {
	Angle    float32
	Position Position
}

type Track struct {
	Id            string
	Lanes         []Lane
	Name          string
	Pieces        []Piece
	StartingPoint StartingPoint
}

type Race struct {
	Cars        []Car
	RaceSession RaceSession
	Track       Track
}

type GameInit struct {
	Race Race
}

type GameInitMsg struct {
	GameInit GameInit
	GameId   gameId
	MsgType  msgType
}
type CarLane struct {
	EndLaneIndex   uint8
	StartLaneIndex uint8
}
type PiecePosition struct {
	InPieceDistance float32
	Lane            CarLane
	Lap             int32
	PieceIndex      int32
}

type CarPosition struct {
	Angle         float32
	Id            CarId
	PiecePosition PiecePosition
}

type CarPositionsMsg struct {
	Data    []CarPosition
	GameId  gameId
	MsgType msgType
}

type Turbo struct {
	TurboDurationMilliseconds uint32
	TurboDurationTicks        uint32
	TurboFactor               uint32
}

type TurboAvailableMsg struct {
	Data     Turbo
	GameId   string
	GameTick uint32
	MsgType  msgType
}
