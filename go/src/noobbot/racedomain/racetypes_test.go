package racedomain

import (
	"encoding/json"
	"noobbot/racedomain"
	"testing"
)

func parse_json_to_struct(input string, json_struct interface{}) (err error) {

	err = json.Unmarshal([]byte(input), &json_struct)
	if err != nil {
		return
	}
	return
}

func dimensionsAssertions(dimensions *racedomain.Dimensions, t *testing.T) {
	if dimensions.GuideFlagPosition != 10 {
		t.Errorf("GuidedFalgPositon is not 10, got %v", dimensions.GuideFlagPosition)
	}

	if dimensions.Length != 40 {
		t.Errorf("Length is not 40, got %v", dimensions.Length)
	}

	if dimensions.Width != 20 {
		t.Errorf("Width is not 40, got %v", dimensions.Width)
	}
}
func TestDecodeDimensions(t *testing.T) {
	var dimensionsStr string

	dimensions := &racedomain.Dimensions{}

	dimensionsStr = `{
                        "guideFlagPosition": 10,
                        "length": 40,
                        "width": 20
                    }`
	err := parse_json_to_struct(dimensionsStr, dimensions)

	if err != nil {
		t.Fatalf("Error decoding Dimensions: %v", err)
	}

	dimensionsAssertions(dimensions, t)
}

func carIdAssertions(carId *racedomain.CarId, t *testing.T) {
	if carId.Color != "red" {
		t.Errorf("Car color is not red, got %v", carId.Color)
	}

	if carId.Name != "toffen" {
		t.Errorf("Car name is not toffen, got %v", carId.Name)
	}
}

func TestDecodeCarId(t *testing.T) {
	carIdStr := `{"color": "red",
                  "name": "toffen"
                 }`

	carId := &racedomain.CarId{}

	err := parse_json_to_struct(carIdStr, carId)

	if err != nil {
		t.Fatalf("Error decoding CarId: %v", err)
	}

	carIdAssertions(carId, t)
}

func TestDecodeCarIds(t *testing.T) {
	carIdsStr := `[{"color": "red",
                  "name": "toffen"
                 },{"color": "red",
                  "name": "toffen"
                 }]`

	carIds := []*racedomain.CarId{}

	err := parse_json_to_struct(carIdsStr, carIds)

	if err != nil {
		t.Fatalf("Error decoding CarIds: %v", err)
	}

	t.Logf("CarIds %v", carIds)
}

func carAssertions(car *racedomain.Car, t *testing.T) {
	dimensionsAssertions(&car.Dimensions, t)
	carIdAssertions(&car.Id, t)
}

func TestDecodeCar(t *testing.T) {
	carStr := `{
                    "dimensions": {
                        "guideFlagPosition": 10,
                        "length": 40,
                        "width": 20
                    },
                    "id": {
                        "color": "red",
                        "name": "toffen"
                    }
                }`

	car := &racedomain.Car{}

	err := parse_json_to_struct(carStr, car)

	if err != nil {
		t.Fatalf("Error decoding Car: %v", err)
	}

	carAssertions(car, t)
}

func TestDecodeCars(t *testing.T) {
	carsStr := `[
                {
                    "dimensions": {
                        "guideFlagPosition": 10,
                        "length": 40,
                        "width": 20
                    },
                    "id": {
                        "color": "red",
                        "name": "toffen"
                    }
                },
                {
                    "dimensions": {
                        "guideFlagPosition": 10,
                        "length": 40,
                        "width": 20
                    },
                    "id": {
                        "color": "blue",
                        "name": "toffenInterior"
                    }
                }
            ]`

	cars := []*racedomain.Car{}

	err := parse_json_to_struct(carsStr, cars)

	if err != nil {
		t.Fatalf("Error decoding Cars: %v", err)
	}
	t.Log("Testing cars %v", cars)
	for i := range cars {
		t.Log("Car %v is %v", i, cars[i])
		carAssertions(cars[i], t)
	}

}

func TestDecodeGameInit(t *testing.T) {
	var gameInitStr string

	gameInitStr = `{"data":{"race":{"cars":[{"dimensions":{"guideFlagPosition":10,"length":40,"width":20},"id":{"color":"red","name":"toffen"}},{"dimensions":{"guideFlagPosition":10,"length":40,"width":20},"id":{"color":"blue","name":"toffenInterior"}}],"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true},"track":{"id":"keimola","lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"name":"Keimola","pieces":[{"length":100},{"length":100},{"length":100},{"length":100,"switch":true},{"angle":45,"radius":100},{"angle":45,"radius":100},{"angle":45,"radius":100},{"angle":45,"radius":100},{"angle":22.5,"radius":200,"switch":true},{"length":100},{"length":100},{"angle":-22.5,"radius":200},{"length":100},{"length":100,"switch":true},{"angle":-45,"radius":100},{"angle":-45,"radius":100},{"angle":-45,"radius":100},{"angle":-45,"radius":100},{"length":100,"switch":true},{"angle":45,"radius":100},{"angle":45,"radius":100},{"angle":45,"radius":100},{"angle":45,"radius":100},{"angle":22.5,"radius":200},{"angle":-22.5,"radius":200},{"length":100,"switch":true},{"angle":45,"radius":100},{"angle":45,"radius":100},{"length":62},{"angle":-45,"radius":100,"switch":true},{"angle":-45,"radius":100},{"angle":45,"radius":100},{"angle":45,"radius":100},{"angle":45,"radius":100},{"angle":45,"radius":100},{"length":100,"switch":true},{"length":100},{"length":100},{"length":100},{"length":90}],"startingPoint":{"angle":90,"position":{"x":-300,"y":-44}}}}},"gameId":"26bc58c9-f9ed-470e-805d-4a40e8d09bd9","msgType":"gameInit"}`

	GameInitMsg := &racedomain.GameInitMsg{}
	err := parse_json_to_struct(gameInitStr, GameInitMsg)

	if err != nil {
		t.Fatalf("Error decoding GameInit: %v", err)
	}

	t.Log("GameInit is: %v", GameInitMsg)

}
